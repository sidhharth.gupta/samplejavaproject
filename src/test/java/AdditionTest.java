package samplejavaproject;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AdditionTest {

	@Test
	void test1() {
		/**
		 *  Steps to test the Java Code
		 *  1. Create the instance of the class to test
		 *  2. Initilize the expected values
		 *  3. Fetch the result value of the code
		 *  4. Compare the value fetched from the code and expected value by assertion method
		 */
		Addition add = new Addition();
		int expected_value = 2;
		int result_value = add.addTwo(1, 1);
		assertEquals(expected_value, result_value, "Adding two numbers");
	}

	@Test
	void test2() {
		/**
		 *  Steps to test the Java Code
		 *  1. Create the instance of the class to test
		 *  2. Initilize the expected values
		 *  3. Fetch the result value of the code
		 *  4. Compare the value fetched from the code and expected value by assertion method
		 */
		Addition add = new Addition();
		int expected_value = 3;
		int result_value = add.addTwo(2, 1);
		assertEquals(expected_value, result_value, "Adding two numbers");
	}

	@Test
	void test3() {
		int[] a = {1,2,3,4,5};
		Addition object1 = new Addition();
		assertArrayEquals(a, object1.Input(a), "Checking the array containts");
	}
}
